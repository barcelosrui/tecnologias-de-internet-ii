﻿using System.Web;
using System.Web.Mvc;

namespace TI_II__TF_HOTEL
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
