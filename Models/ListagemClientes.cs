﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace TI_II__TF_HOTEL.Models
{
    public class ListagemClientes
    {
        public int ClienteID { get; set; }
        [Display (Name="Cliente")]
        public string Nome { get; set; }
        public string Apelido { get; set; }
        [Display(Name = "Morada")]
        public string Rua { get; set; }
        public string NPorta { get; set; }
        public string Andar { get; set; }
        public string Lado { get; set; }
        [Display (Name ="Código Postal")]
        public string CPostal { get; set; }
        [Display(Name = "Localidade")]
        public string Localidade { get; set; }
        [Display(Name = "Contacto")]
        public string Contacto { get; set; }
        [Display(Name = "BI/CC")]
        public string BI { get; set; }
        public string NIF { get; set; }
        [Display(Name = "E-Mail")]
        public string EMAIL { get; set; }
    }
}