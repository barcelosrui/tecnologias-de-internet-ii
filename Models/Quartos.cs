namespace TI_II__TF_HOTEL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web;
    public partial class Quartos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Quartos()
        {
            Reservas = new HashSet<Reservas>();
        }

        [Key]
        public int QuartoID { get; set; }

        [Display(Name ="Categoria Quarto")]
        public string TipoQuarto { get; set; }


        [Required]
        [Display(Name = "Quantidade")]
        public int Quantidade { get; set; }
        [Required]
        [Display(Name = "Pre�o")]
        public float Preco { get; set; }
        [Display(Name = "Descri��o do Quarto")]
        public string Descricao { get; set; }
        public string ImagePath { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reservas> Reservas { get; set; }
    }
}
