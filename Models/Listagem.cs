﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TI_II__TF_HOTEL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class Listagem
    {
        [Display (Name ="Id Reserva")]
        public int ReservaID { get; set; }
        [Display(Name = "Data de Entrada")]
        public DateTime DataEnt { get; set; }
        [Display(Name = "Data de Saída")]
        public DateTime DataSai { get; set; }
        [Display(Name = "Data de Registo")]
        public DateTime DataReg { get; set; }
        [Display(Name = "Categoria")]
        public string Quarto { get; set; }
        [Display(Name = "Cliente")]
        public string Nome { get; set; }
        public string Apelido { get; set; }
        [Display(Name = "Preço")]
        public float PrecoTotal { get; set; }

    }
}