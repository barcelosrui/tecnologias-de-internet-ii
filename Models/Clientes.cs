namespace TI_II__TF_HOTEL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Clientes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Clientes()
        {
            Reservas = new HashSet<Reservas>();
        }

        [Key]
        public int ClienteID { get; set; }

        [Required]
        [RegularExpression("[0-9]{9}", ErrorMessage = "O NIF tem de ter 9 caracteres Num�ricos")]
        [Display(Name = "NIF: ")]
        public string NIF { get; set; }

        [Required]
        [Display(Name = "Nome: ")]
        public string NomeProprio { get; set; }

        [Required]
        [Display(Name = "Apelido: ")]
        public string Apelido { get; set; }

        [Required]
        [RegularExpression("[0-9]{8}", ErrorMessage = "O BI/CC tem de ter 8 caracteres Num�ricos, caso tenha 7 insira um zero � esquerda")]
        [Display(Name = "BI/CC: ")]
        public string BI { get; set; }

        [Required]
        [Display(Name = "Morada: ")]
        public string Rua { get; set; }
        [Display(Name = "N�.: ")]
        public string nPorta { get; set; }
        [Display(Name = "Andar: ")]
        public string Andar { get; set; }
        [Display(Name = "Lado: ")]
        public string Lado { get; set; }
        [Required]
        [RegularExpression("[0-9]{4}-[0-9]{3}", ErrorMessage ="O C�digo de Postal tem de ser escrito da seguinte forma XXXX-XXX")]
        [Display(Name = "C�digo Postal: ")]
        public string cPostal { get; set; }
        [Required]
        [Display(Name = "Localidade: ")]
        public string Localidade { get; set; }
        [Required]
        [Display(Name = "Telem�vel: ")]
        [RegularExpression("[0-9]{9}", ErrorMessage = "O Contacto � composto por 9 caracteres Num�ricos")]
        public string Contacto { get; set; }
 
        public string UserID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reservas> Reservas { get; set; }
    }
}
