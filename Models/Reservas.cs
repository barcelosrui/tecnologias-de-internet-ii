namespace TI_II__TF_HOTEL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Reservas
    {
        [Key]
        public int ReservaID { get; set; }
        [Display(Name = "Data de Entrada")]
        [Column(TypeName = "date")]
        public DateTime DataEntrada { get; set; }

        [Required]
        [Column(TypeName = "date")]
        [Display(Name = "Data de Sa�da")]
        public DateTime DataSaida { get; set; }

        [Required]
        [Column(TypeName = "date")]
        [Display(Name = "Data de Reserva")]
        public DateTime DataReg { get; set; }
        [Required]
        [Display(Name = "Pre�o Total")]
        public float PrecoTotal { get; set; }
        [Display(Name = "Cliente")]
        public int ClienteFK { get; set; }
        public virtual Clientes Clientes { get; set; }
        [Display(Name ="Quarto")]
        public int QuartoFK { get; set; }
        public virtual Quartos Quartos { get; set; }
    }
   
}
