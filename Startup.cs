﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TI_II__TF_HOTEL.Startup))]
namespace TI_II__TF_HOTEL
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
