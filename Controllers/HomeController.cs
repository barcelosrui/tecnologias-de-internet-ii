﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TI_II__TF_HOTEL.Models;

namespace TI_II__TF_HOTEL.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View(db.Quartos.ToList().OrderBy(n => n.Preco).Take(1));
        }
        public ActionResult notFound()
        {
            return RedirectToAction("Index", "Home");
            //return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Veja aqui os Contactos.";

            return View();
        }
        public ActionResult Hotel()
        {
            ViewBag.Message = "Saiba quem somos.";

            return View();
        }
    }
}