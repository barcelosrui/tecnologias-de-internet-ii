﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TI_II__TF_HOTEL.Models;

namespace TI_II__TF_HOTEL.Controllers
{
    public class ClientesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Clientes
        [Authorize]
        public ActionResult Perfil()
        {
            var userid = User.Identity.GetUserId();
            var user = (from c in db.Clientes where c.UserID == userid select c).Single();
            ViewBag.cliente = user;
            return View(user);
        }
        [Authorize(Roles = "Funcionarios")]
        public ActionResult Listagem()
        {
            IEnumerable<ListagemClientes> cliente =
                from c in db.Clientes
                join u in db.Users on c.UserID equals u.Id
                select new ListagemClientes
                {
                    ClienteID = c.ClienteID,
                    Nome = c.NomeProprio,
                    Apelido = c.Apelido,
                    Contacto = c.Contacto,
                    Rua = c.Rua,
                    NPorta = c.nPorta,
                    Andar = c.Andar,
                    Lado = c.Lado,
                    CPostal = c.cPostal,
                    Localidade = c.Localidade,
                    BI = c.BI,
                    NIF = c.NIF,
                    EMAIL = u.Email,
                };
            return View(cliente.ToList());
        }
        public ActionResult Index()
        {
            return RedirectToAction("Perfil");
        }

        // GET: Clientes/Details/5
        [Authorize(Roles = "Funcionarios")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Clientes clientes = db.Clientes.Find(id);
            if (clientes == null)
            {
                return RedirectToAction("Index");
            }
            return View(clientes);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            return PartialView();
        }
        
        // GET: Clientes/Edit/5
        [Authorize(Roles = "Funcionarios")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Clientes clientes = db.Clientes.Find(id);
            if (clientes == null)
            {
                return RedirectToAction("Index");
            }
            return View(clientes);
        }
        
        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Funcionarios")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClienteID,NIF,NomeProprio,Apelido,BI,Rua,nPorta,Andar,Lado,cPostal,Localidade,Contacto,UserID")] Clientes clientes)
        {
            TempData["cl"] = "Alterar Perfil";
            if (ModelState.IsValid)
            {
                TempData["clienteSuccess"] = "Perfil Alterado com sucesso!";
                db.Entry(clientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details",  new { id = clientes.ClienteID });
            }
            TempData["clienteErro"] = "Verifique se introduziu bem os dados!";
            return View(clientes);
        }

        // GET: Clientes/Edit/5
        [Authorize]
        public ActionResult Editar()
        {
            var userid = User.Identity.GetUserId();
            var user = (from c in db.Clientes where c.UserID == userid select c.ClienteID).Single();
            ViewBag.cliente = user;
            Clientes clientes = db.Clientes.Find(user);
            if (clientes == null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(clientes);
        }
        
        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Funcionarios")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "ClienteID,NIF,NomeProprio,Apelido,BI,Rua,nPorta,Andar,Lado,cPostal,Localidade,Contacto,UserID")] Clientes clientes)
        {
            TempData["cl"] = "Alterar Perfil";
            clientes.UserID = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                TempData["clienteSuccess"] = "Perfil Alterado com sucesso!";
                db.Entry(clientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Perfil");
            }
            TempData["clienteErro"] = "Verifique se introduziu bem os dados!";
            return View(clientes);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
