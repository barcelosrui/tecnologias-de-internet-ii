﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TI_II__TF_HOTEL.Models;

namespace TI_II__TF_HOTEL.Controllers
{
    public class ReservasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Reservas
        [Authorize]
        public ActionResult Index(){
            var userid = User.Identity.GetUserId();
            var user = (from c in db.Clientes where c.UserID == userid select c.ClienteID).Single();
            IEnumerable<Listagem> reservas =
                from r in db.Reservas
                join q in db.Quartos on r.QuartoFK equals q.QuartoID
                where r.ClienteFK == user
                select new Listagem
                {
                    ReservaID = r.ReservaID,
                    DataEnt = r.DataEntrada,
                    DataSai = r.DataSaida,
                    DataReg = r.DataReg,
                    Quarto = q.TipoQuarto,
                    PrecoTotal = r.PrecoTotal
                };
            return View(reservas.ToList().OrderByDescending(r => r.DataReg));
        }
        //Lista
        [Authorize(Roles = "Funcionarios")]
        public ActionResult Listagem(){
            IEnumerable<Listagem> reservas =
                from r in db.Reservas
                join q in db.Quartos on r.QuartoFK equals q.QuartoID
                join c in db.Clientes on r.ClienteFK equals c.ClienteID
                select new Listagem
                {
                    ReservaID = r.ReservaID,
                    DataEnt = r.DataEntrada,
                    DataSai = r.DataSaida,
                    DataReg = r.DataReg,
                    Quarto = q.TipoQuarto,
                    Nome = c.NomeProprio,
                    Apelido = c.Apelido,
                    PrecoTotal = r.PrecoTotal
                };

            return View(reservas.ToList().OrderByDescending(r=>r.DataReg));
        }
        // GET: Reservas/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            
            if (id == null){
                return RedirectToAction("Index", "Home");
            }
            Reservas reservas = db.Reservas.Find(id);
            
            if (reservas == null){
                return RedirectToAction("Index", "Home");
            }
            var tquarto = (from q in db.Quartos where reservas.QuartoFK == q.QuartoID select q.TipoQuarto).Single();
            var nome = (from c in db.Clientes where reservas.ClienteFK == c.ClienteID select c.NomeProprio).Single();
            var apelido = (from c in db.Clientes where reservas.ClienteFK == c.ClienteID select c.Apelido).Single();
            //var apelido = (from c in db.Clientes where reservas.ClienteFK == c.ClienteID select c.Apelido).Single();
            
            ViewBag.nome = nome;
            ViewBag.apelido = apelido;
            ViewBag.tipo = tquarto;
            return View(reservas);
            
        }
        // GET: Reservas/Create
        [Authorize]
        public ActionResult CriarReserva()
        {
            
            ViewBag.QuartoFK = new SelectList(db.Quartos, "QuartoID", "TipoQuarto");
            return View();
        }

        // POST: Reservas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CriarReserva([Bind(Include = "ReservaID,DataEntrada,DataSaida,DataReg,ClienteFK,QuartoFK")] Reservas reserva)
        {
            var userid = User.Identity.GetUserId();
            //******************************************************
            //determinar se o quarto pretendido está disponível

            // num. de unidades do tipo de quarto pretendido
            int numQuartos = (from q in db.Quartos where q.QuartoID == reserva.QuartoFK select q.Quantidade).FirstOrDefault();
            // num. de reservas ativas para este tipo de quarto
            int numReservas = (from r in db.Reservas
                               where (r.QuartoFK == reserva.QuartoFK &&
                                     (r.DataEntrada <= reserva.DataEntrada && r.DataSaida >= reserva.DataEntrada ||
                                      r.DataEntrada < reserva.DataSaida && r.DataSaida >= reserva.DataSaida))
                               select r.ReservaID).Count();
            // posso fazer a reserva?
            if ((numQuartos > numReservas) && (reserva.DataSaida> reserva.DataEntrada))
            { // posso fazer a reserva
                var prec = (from p in db.Quartos where p.QuartoID == reserva.QuartoFK select p.Preco).FirstOrDefault();
                var dent = reserva.DataEntrada;
                var dsai = reserva.DataSaida;
                TimeSpan numDias = (dsai - dent);
                int dias = numDias.Days;
                var totprec = dias * prec;
                try
                {
            // recupera o ID do utilizador
                    reserva.ClienteFK = (from c in db.Clientes where c.UserID == userid select c.ClienteID).Single();
                    // regista a data em q foi efetuada a reserva 
                    reserva.DataReg = DateTime.Now;
                    // determina o ID da reserva
                    int newID = (from rr in db.Reservas orderby rr.ReservaID descending select rr.ReservaID).FirstOrDefault() + 1;
                    reserva.ReservaID = newID;
                    //determina o preço da reserva
                    reserva.PrecoTotal = totprec;
                    TempData["pt"] = totprec;
                    if (ModelState.IsValid)
                    {
                        db.Reservas.Add(reserva);
                        db.SaveChanges();
                        // falta notificar utilizador q a reserva foi efetuada com sucesso
                        TempData["ReservaSuccess"] = "Reserva efectuada com sucesso!";
                        return RedirectToAction("Details", new { id = newID});
                    }
                }
                catch (Exception)
                {
                    //falta fazer algo se houver erro;
                    ModelState.AddModelError("", string.Format("Ups! Ocorreu um erro!"));
                    return View(reserva);
                }
            }
            // informar q o quarto não está disponível
            TempData["ReservaErro"] = "Quarto não disponivel para a data pertendida, Tente Outra Opção!";
            //ModelState.AddModelError("", string.Format("Quarto não disponivel para a data pertendida, Tente Outra Opção!"));
            ViewBag.QuartoFK = new SelectList(db.Quartos, "QuartoID", "TipoQuarto", reserva.QuartoFK);
            return View(reserva);
        }

        // GET: Reservas/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            ViewBag.QuartoFK = new SelectList(db.Quartos, "QuartoID", "TipoQuarto");
            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }
            Reservas reservas = db.Reservas.Find(id);
            if (reservas == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(reservas);
        }
        
        // POST: Reservas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "ReservaID,DataEntrada,DataSaida,DataReg,ClienteFK,QuartoFK")] Reservas reserva)
        {
            var userid = User.Identity.GetUserId();
            //******************************************************
            //determinar se o quarto pretendido está disponível
            // num. de unidades do tipo de quarto pretendido
            int numQuartos = (from q in db.Quartos where q.QuartoID == reserva.QuartoFK select q.Quantidade).FirstOrDefault();
            // num. de reservas ativas para este tipo de quarto
            int numReservas = (from r in db.Reservas
                               where (r.QuartoFK == reserva.QuartoFK &&
                                     (r.DataEntrada <= reserva.DataEntrada && r.DataSaida >= reserva.DataEntrada ||
                                      r.DataEntrada < reserva.DataSaida && r.DataSaida >= reserva.DataSaida))
                               select r.ReservaID).Count();
            numReservas += 1;
            // posso fazer a reserva?
            if ((numQuartos > numReservas) && (reserva.DataSaida > reserva.DataEntrada))
            { // posso fazer a reserva
                try
                {
                    var prec = (from p in db.Quartos where p.QuartoID == reserva.QuartoFK select p.Preco).FirstOrDefault();
                    var dent = reserva.DataEntrada;
                    var dsai = reserva.DataSaida;
                    TimeSpan numDias = (dsai - dent);
                    int dias = numDias.Days;
                    var totprec = dias * prec;
                    //// recupera o ID do utilizador
                    //reserva.ClienteFK = (from c in db.Clientes where c.UserID == userid select c.ClienteID).Single();
                    //// regista a data em q foi efetuada a reserva 
                    //reserva.DataReg = DateTime.Now;
                    //determina o preço da reserva
                    reserva.PrecoTotal = totprec;
                    TempData["pt"] = totprec;
                    if (ModelState.IsValid)
                    {
                        db.Entry(reserva).State = EntityState.Modified;
                        db.SaveChanges();
                        // falta notificar utilizador q a reserva foi efetuada com sucesso
                        TempData["ReservaSuccess"] = "Reserva Alterada com sucesso!";
                        return RedirectToAction("Details", new { id = reserva.ReservaID});
                    }
                }
                catch (Exception)
                {
                    //falta fazer algo se houver erro;
                    ModelState.AddModelError("", string.Format("Ups! Ocorreu um erro!"));
                    return View(reserva);
                }

            }
            // informar q o quarto não está disponível
            TempData["ReservaErro"] = "Quarto não disponivel para a data pertendida, Tente Outra Opção!";
            ViewBag.QuartoFK = new SelectList(db.Quartos, "QuartoID", "TipoQuarto", reserva.QuartoFK);
            return View(reserva);
        }

        // GET: Reservas/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }
            Reservas reservas = db.Reservas.Find(id);
            if (reservas == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(reservas);
        }
        [Authorize]
        // POST: Reservas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reservas reserva = db.Reservas.Find(id);
            try
            {

                db.Reservas.Remove(reserva);
                db.SaveChanges();
                TempData["ReservaSuccess"] = "Reserva Eliminada com sucesso!";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["ReservaErro"] = "Reserva Não Eliminada!";
                //falta fazer algo se houver erro;
                ModelState.AddModelError("", string.Format("Ups! Ocorreu um erro!"));
                return View(reserva);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
