﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TI_II__TF_HOTEL.Models;

namespace TI_II__TF_HOTEL.Controllers
{
    public class QuartosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Quartos        
        public ActionResult Index()
        {
            return View(db.Quartos.OrderByDescending(m => m.Preco).ToList());
        }

        // GET: Quartos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }
            Quartos quartos = db.Quartos.Find(id);
            if (quartos == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var nomeIMG = (from q in db.Quartos where id == q.QuartoID select q.ImagePath).Single();
            if (nomeIMG == null)
            {
                ViewBag.img = "image-not-found.jpg";
            }
            else
            {
                ViewBag.img = nomeIMG;
            }
            return View(quartos);
        }

        // GET: Quartos/Create
        [Authorize(Roles = "Funcionarios")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Quartos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Funcionarios")]
        public ActionResult Create([Bind(Include = "QuartoID,TipoQuarto,Quantidade,Preco,Descricao,ImagePath")]Quartos quartos ,HttpPostedFileBase file)
        {
            TempData["AQ"] = "Categoria de Quartos";
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    file.SaveAs(HttpContext.Server.MapPath("~/Images/")
                                                          + file.FileName);
                    quartos.ImagePath = file.FileName;
                }
                db.Quartos.Add(quartos);
                db.SaveChanges();
                TempData["AQSuccess"] = "Categoria de Quarto Acrescentado com Sucesso!";
                return RedirectToAction("Index");
            }
            TempData["AQErro"] = "Verifique se introduziu bem os dados!";
            return View(quartos);
        }
        [Authorize(Roles = "Funcionarios")]
        // GET: Quartos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }
            Quartos quartos = db.Quartos.Find(id);
            if (quartos == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(quartos);
        }
        [Authorize(Roles = "Funcionarios")]
        // POST: Quartos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Funcionarios")]
        public ActionResult Edit([Bind(Include = "QuartoID,TipoQuarto,Quantidade,Preco,Descricao,ImagePath")] Quartos quartos, HttpPostedFileBase file)
        {
            TempData["AQ"] = "Categoria de Quartos";
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    file.SaveAs(HttpContext.Server.MapPath("~/Images/")
                                                          + file.FileName);
                    quartos.ImagePath = file.FileName;
                }
                db.Entry(quartos).State = EntityState.Modified;
                db.SaveChanges();
                TempData["AQSuccess"] = "Categoria de Quarto alterado com Sucesso!";
                return RedirectToAction("Index");
            }
            TempData["AQErro"] = "Verifique se introduziu bem os dados!";
            return View(quartos);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
